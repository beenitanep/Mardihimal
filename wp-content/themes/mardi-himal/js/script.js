(function($){
	$(document).ready(function(){
		
	// Magnific popup
		//-----------------------------------------------
		if (($(".popup-img").length > 0) || ($(".popup-iframe").length > 0) || ($(".popup-img-single").length > 0)) { 		
			$(".popup-img").magnificPopup({
				type:"image",
				gallery: {
					enabled: true,
				}
			});
			$(".popup-img-single").magnificPopup({
				type:"image",
				gallery: {
					enabled: false,
				}
			});
			$('.popup-iframe').magnificPopup({
				disableOn: 700,
				type: 'iframe',
				preloader: false,
				fixedContentPos: false
			});
		};	
		 
	$('[data-toggle="tooltip"]').tooltip()
	$( ".date-book" ).datepicker();
	
	$('#roomCategory').multiselect({
		includeSelectAllOption: true,
		nonSelectedText: 'Room Type',
		numberDisplayed: 1
	});
	 
	$('#select_no').multiselect({
		nonSelectedText: 'Room Type',
		inputType: 'radio',
		numberDisplayed: 1
	});
	$('#adults').multiselect({
		nonSelectedText: 'No. of Adults',
		
	});
	$('#children').multiselect({
		nonSelectedText: 'No. of Child',
		
	});
	$('#nroom').multiselect({
		nonSelectedText: 'No. of Room',
		
	});
	$( ".datepicker" ).datepicker({
		numberOfMonths: 2	
	});
	$('#two-inputs').dateRangePicker(
	{
		separator : ' to ',
		customArrowPrevSymbol: '<i class="fa fa-angle-left"></i>',
		customArrowNextSymbol: '<i class="fa fa-angle-right"></i>',
		getValue: function()
		{
			if ($('#date-range200').val() && $('#date-range201').val() )
				return $('#date-range200').val() + ' CHECK OUT' + $('#date-range201').val();
			else
				return '';
		},
		setValue: function(s,s1,s2)
		{
			$('#date-range200').val(s1);
			$('#date-range201').val(s2);
		}
	});
	//Scroll totop
//-----------------------------------------------
        $(window).scroll(function () {
            if ($(this).scrollTop() != 0) {
                $(".scrollToTop").fadeIn();
            } else {
                $(".scrollToTop").fadeOut();
            }
        });

        $(".scrollToTop").click(function () {
            $("body,html").animate({ scrollTop: 0 }, 800);
        });
	
	//OWL CARASOUEL//
	var owl = $('.owl-carousel');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:15,
	dots: true,
    autoplay:true,
	autoplayTimeout:3000,
    autoplayHoverPause:true
});
var owl = $('.testimonial');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:15,
	dots: true,
    autoplay:true,
	autoplayTimeout:3000,
    autoplayHoverPause:true
});
// Isotope filters
		//-----------------------------------------------
		if ($('.isotope-container').length>0 || $('.masonry-grid').length>0 || $('.masonry-grid-fitrows').length>0) {
			$(window).load(function() {
				$('.masonry-grid').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'masonry'
				});
				$('.masonry-grid-fitrows').isotope({
					itemSelector: '.masonry-grid-item',
					layoutMode: 'fitRows'
				});
				$('.isotope-container').fadeIn();
				var $container = $('.isotope-container').isotope({
					itemSelector: '.isotope-item',
					layoutMode: 'masonry',
					transitionDuration: '0.6s',
					filter: "*"
				});
				// filter items on button click
				$('.filters').on( 'click', 'ul.nav li a', function() {
					var filterValue = $(this).attr('data-filter');
					$(".filters").find("li.active").removeClass("active");
					$(this).parent().addClass("active");
					$container.isotope({ filter: filterValue });
					return false;
				});
			});
		};

		

		

		
			

	}); // End document ready

})(this.jQuery);










