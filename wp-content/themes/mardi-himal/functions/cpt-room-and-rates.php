<?php 
/*=======================================================================*/
// Room and rates  Post Type
/*=======================================================================*/
add_action('init', 'register_room_and_rates');
function register_room_and_rates(){
	$labels = array(
		'name' => _x('Room Rates', 'post type general name'),
		'singular_name' => _x('Room Rates', 'post type singular name'),
		'add_new' => _x('Add New', 'Room Rates'),
		'add_new_item' => __('Room Rates'),
		'edit_item' => __('Edit Room Rates'),
		'new_item' => __('New Room Rates'),
		'view_item' => __('View Room Rates'),
		'search_items' => __('Search Room Rates'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title', 'editor','thumbnail')
				);
	register_post_type('room-and-rates' , $args);
}
?>