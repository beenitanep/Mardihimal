<?php 
/*=======================================================================*/
// Service  Post Type
/*=======================================================================*/
add_action('init', 'register_service');
function register_service(){
	$labels = array(
		'name' => _x('Service', 'post type general name'),
		'singular_name' => _x('Service', 'post type singular name'),
		'add_new' => _x('Add New', 'Service'),
		'add_new_item' => __('Service'),
		'edit_item' => __('Edit Service'),
		'new_item' => __('New Service'),
		'view_item' => __('View Service'),
		'search_items' => __('Search Service'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title', 'editor','thumbnail')
				);
	register_post_type('service' , $args);
}
?>