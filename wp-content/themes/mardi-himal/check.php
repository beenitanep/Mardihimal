<?php 
include('../../../wp-config.php');
include("header.php")?>
  <div class="page-top parallax dark-translucent">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="page-title">
            <h2>Check</h2>
            <span class="seperator"> <i></i> <i class="active"></i> <i></i> </span> </div>
          <ol class="breadcrumb">
            <li><a href="index.php">Home</a></li>
            <li class="active">Check</li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <section class="innerpage about-content">
    <div class="container">
      <ul class="booking-type">
        <li class="active"><a href="#"><span>1</span>Select Available Rooms</a></li>
        <li><a href="#"><span>2</span>Register Personal Details</a></li>
        <li><a href="#"><span>3</span>Booking Done</a></li>
      </ul>
      <div class="reservation_form">
        <form>
          <div class="form-group">
            <label>Check In Date</label>
            <input type="text" class="form-control datepicker" id="arrival-date" placeholder="01/04/2017">
          </div>
          <div class="form-group">
            <label>Check Out Date</label>
            <input type="text" class="form-control datepicker" id="departure-date" placeholder="01/07/2017">
          </div>
        </form>
      </div>
      <form>
        <div class="table-responsive reserve-type">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th></th>
                <th class="rt">Room Type</th>
                <th>Room</th>
                <th>Adults</th>
                <th>Child</th>
                <th>Price($)</th>
                <th>Total($)</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type1"><a href="#"><strong>V.I.P Rooms</strong></a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room1" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person1 form-control" id="adult1" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select></td>
                <td><select class="drop person1 form-control" id="child1" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                  </select></td>
                <td><span id="room_price1">60</span> 
                  <!--                               --></td>
                <td></td>
              </tr>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type2"><a href="#"> <strong>Suite Rooms</strong> </a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room2" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person2 form-control" id="adult2" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select></td>
                <td><select class="drop person2 form-control" id="child2" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                  </select></td>
                <td><span id="room_price2">60</span> 
                  <!--                               --></td>
                <td><span id="total2" class="total">60</span></td>
              </tr>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type3"><a href="#"><strong>Deluxe Rooms</strong></a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room3" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person3 form-control" id="adult3" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><select class="drop person3 form-control" id="child3" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><span id="room_price3">50</span> 
                  <!--                               --></td>
                <td></td>
              </tr>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type4"><a href="#"><strong>STANDARD BEDROOM</strong></a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room4" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person4 form-control" id="adult4" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><select class="drop person4 form-control" id="child4" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><span id="room_price4">50</span> 
                  <!--                               --></td>
                <td></td>
              </tr>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type5"><a href="#"><strong>Double Bedroom</strong></a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room5" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person5 form-control" id="adult5" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><select class="drop person5 form-control" id="child5" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><span id="room_price5">50</span> 
                  <!--                               --></td>
                <td></td>
              </tr>
              <tr>
                <td><div class="checkbox">
                    <label>
                      <input type="checkbox">
                    </label>
                  </div></td>
                <td><span id="room_type6"><a href="#"><strong>Single Bedroom</strong></a></span></td>
                <!--                                                        <td>--><!--</td>-->
                <td><select id="room6" class="drop form-control" name="room">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                  </select></td>
                <td><select class="drop person6 form-control" id="adult6" name="adults">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><select class="drop person6 form-control" id="child6" name="chi">
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                  </select></td>
                <td><span id="room_price6">50</span></td>
                <td></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th colspan="6">Subtotal * Nights</th>
                <th> <span id="mul_nights">300.00</span> 
                  <!--                        <input type="text" id="mul_nights">--> </th>
              </tr>
              <tr>
                <th colspan="6">Service Charge(10%)</th>
                <th> <span id="with_service">330.00</span> 
                  <!--                        <input type="text" id="with_service">--> </th>
              </tr>
              <tr>
                <th colspan="6">Tax(13%)</th>
                <th> <span id="with_tax">372.90</span> 
                  <!--                        <input type="text" id="with_tax">--> </th>
              </tr>
            </tfoot>
          </table>
        </div>
        <button type="submit" class="btn btn-info">Submit</button>
      </form>
    </div>
  </section>
  <?php include("footer.php")?>
