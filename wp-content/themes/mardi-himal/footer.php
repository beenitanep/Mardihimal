<!--Footer Start-->
<footer id="page-footer" class="textured-gradient ">
  <div class="container">
    <div class="row clearfix">
      <div class="col-lg-4">
        <h6> Mardi Himal Eco Village
          Hotel & Restaurant</h6>
        <!-- <ul>
          
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Gifts</a> </li>
          <li> <a href="#">Careers</a> </li>
          <li> <a href="#">Contact Us</a> </li>
        </ul> -->
        <?php 
               wp_nav_menu( array (
                            'theme_location'    => 'secondary',
                            'container'         => '',
                            //'menu_id'           => 'top-nav',
                            'depth'             => 0, // set to 1 to disable dropdowns
                            'fallback_cb'       => false,
                            'menu_class'    => ''
                            //'after'   => '|'
                        ));
          ?>
      </div>
      <div class="col-lg-4"><h6>Media</h6>
        <ul>
          <li> <a href="#" target="_blank">Newsroom</a> </li>
          <li> <a href="#">Press Contacts</a> </li>
          <li> <a href="#" target="_blank">Download Brochure</a> </li>
        </ul>
        </div>
     
        
      
      
      <div class="col-lg-4"><form action="#" method="POST" class="newsletter">
        <h6 id="newsletter_signup" tabindex="0">Newsletter Signup</h6>
        <small>Enter your email address below to receive our monthly fun-filled newsletter.</small>
        <div class="form-group">
        	<input type="text" name="newsletter-email" class="form-control" id="email_inputs" placeholder="Email address" aria-invalid="false" aria-labelledby="newsletter_signup" aria-describedby="err_email" aria-required="true">
        </div>
         <button class="btn btn-outline-warning" role="button">Submit</button>
        
      </form></div>
      
      
       </div>
       <div class="bottom_footer text-center">
       	<small>© 2017 Mardi Himal Eco Village
      Hotel & Restaurant. All Rights Reserved.</small>
       </div>
  </div>
</footer>
<!--Footer End--> 
<?php wp_footer(); ?>
</body>
</html>