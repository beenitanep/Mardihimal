<?php 
/*
Template Name: Testimonial
*/
get_header();?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Testimonial</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php bloginfo('url');?>">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Testimonial</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    <ul class="testimonial-wrap">
    <?php
    $args = array(
      'posts_per_page' => -1,
      'post_type' => 'testimonial'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
     $country = get_post_meta( $post->ID, 'country', true );
    ?>
      <li> <span class="avtar"><i class="fa fa-user"></i></span>
        <div class="testimonials"> <div class=" clearfix"><span class="name"> <?php the_title(); if(  $country!= ''){?>,<?php } echo get_post_meta( $post->ID, 'country', true );?> </span> <span class="date"> <?php echo get_the_date('Y-m-d'); ?> <?php the_time( 'H:i:s' ); ?> </span></div>
          <p> <?php echo substr(strip_tags(get_the_content()),0,600);?></p>
        </div>
      </li>
      <?php   
          endwhile; 
           wp_reset_query(); 
  ?>
    </ul>
    <div class="row">
        <div class="col-md-12">
          <div class="review-form clearfix">
            <h2 class="title">Write your own review</h2>
             <?php  echo do_shortcode( '[wpuf_form id="111"]' ); ?>
          </div>
        </div>
      </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

 <?php get_footer();?>